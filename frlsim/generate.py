import frontmatter
import hashlib
from jinja2 import Markup
from markdown import markdown
import os
import subprocess

def generate(core):
	
	# Indexes
	
	core.render('index.html', 'frlsim/index.html')
	core.render('cases/', 'frlsim/cases.html')
	core.render('testing/', 'frlsim/testing.html')
	core.render('vaccination/', 'frlsim/vaccination.html')
	core.render('lgas/', 'frlsim/lga.html')
	core.render('about/', 'frlsim/about.html')
    
    # LGAs
    
	core.render('lgas/lakemacquarie/', 'frlsim/lgas/lakemac.html')
	core.render('lgas/newcastle/', 'frlsim/lgas/newcastle.html')
	core.render('lgas/midcoast/', 'frlsim/lgas/midcoast.html')
	core.render('lgas/maitland/', 'frlsim/lgas/maitland.html')
	core.render('lgas/portstephens/', 'frlsim/lgas/portstephens.html')
	core.render('lgas/cessnock/', 'frlsim/lgas/cessnock.html')
	core.render('lgas/singleton/', 'frlsim/lgas/singleton.html')
	core.render('lgas/muswellbrook/', 'frlsim/lgas/muswellbrook.html')
	core.render('lgas/upperhunter/', 'frlsim/lgas/upperhunter.html')
	core.render('lgas/dungog/', 'frlsim/lgas/dungog.html')
